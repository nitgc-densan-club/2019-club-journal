# 2019年度群馬高専電算部 部誌

第22回工華祭と第43回関東信越地区高等専門学校文化発表会に出品する、群馬高専電算部の部誌『D言語』のリポジトリです。

## 編集者向けガイド

`for_editor.md`を参照してください

## 執筆者向けガイド

`for_writer.md`を参照をしてください

## ダウンロード

工事中

## Build

### Require

* [pLaTeX-ng](https://github.com/clerkma/ptex-ng)(recommend) or [upLaTeX](http://www.t-lab.opal.ne.jp/tex/uptex.html)
* [ptex2pdf](https://texwiki.texjp.org/?ptex2pdf)(if you use upLaTeX)
* [ifptex](https://github.com/zr-tex8r/ifptex/)

* download these package from [CTAN](https://www.ctan.org/).
  * here
  * amsmath
  * amsfonts
  * graphicx
  * xcolor
  * longtable
  * multirow
  * booktabs
  * url
  * hyperref
  * ulem
  * pdfpages(optional)

### Step

1. clone [this repogitory](https://gitlab.com/nitgc-densan-club/2019-club-journal.git).

2. open terminal

3. type and enter `extractbb images/*.png`

4. type and enter `platex-ng main.tex` or `ptex2pdf -u -l -ot -no-guess-input-enc -kanji=utf8 main.tex`.

5. (optional) type and enter `platex-ng main-booklet.tex` or `ptex2pdf -u -l -ot -no-guess-input-enc -kanji=utf8 main-booklet.tex`.

  * make booklet version
