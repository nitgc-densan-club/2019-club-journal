# 神奈川旅行記

採用されていたら部誌の写真の表紙をとってきたsikutuです。(採用されてない可能性微レ存)
梅雨の真っただ中に小雨がたたきつける中撮影しました。カメラはFUJIFILM　X-M1、レンズはフジノン50-230です。
休日お出かけパスを使い神奈川と東京を回った気がします。（あんま覚えてない）
詳しい旅行記と写真はpixivかツイッターあたりに載せるかもしれません。
これ書いてるのテスト期間中で時間がないのでこれ以上のことは省略させていただきます。
なお私の写真が部誌の表紙になっていた場合、著作権は多分群馬高専電算部にあります。
ピクシブ　(https://www.pixiv.net/member.php?id=14000843)
ツイッタ　(https://twitter.com/hiro_sikutu)

\begin{figure}[htb]
    \centering
        \includegraphics[height=160mm]{images/sikutu-01.png}
\end{figure}
\newpage
\begin{figure}[htb]
    \centering
        \includegraphics[width=120mm]{images/sikutu-02.png}
\end{figure}
\begin{figure}[htb]
    \centering
        \includegraphics[width=120mm]{images/sikutu-03.png}
\end{figure}
\newpage
\begin{figure}[htb]
    \centering
        \includegraphics[width=120mm]{images/sikutu-04.png}
\end{figure}
\begin{figure}[htb]
    \centering
        \includegraphics[width=120mm]{images/sikutu-05.png}
\end{figure}
\newpage
\begin{figure}[htb]
    \centering
        \includegraphics[width=120mm]{images/sikutu-06.png}
\end{figure}
\begin{figure}[htb]
    \centering
        \includegraphics[width=120mm]{images/sikutu-07.png}
\end{figure}
\newpage
\begin{figure}[htb]
    \centering
        \includegraphics[width=120mm]{images/sikutu-08.png}
\end{figure}
\begin{figure}[htb]
    \centering
        \includegraphics[width=120mm]{images/sikutu-09.png}
\end{figure}